from __future__ import annotations
from dataclasses import dataclass

from saport.simplex.model import Model
from saport.simplex.tableau import Tableau
from saport.simplex.expressions.expression import Variable
from saport.simplex.exceptions import CalledWithoutAssignmentError

@dataclass
class Solution:
    """
        A class to represent a solution to linear programming problem.


        Attributes
        ----------
        model : smodel.Model
            model corresponding to the solution
        initial_tableau: Tableau
            a simplex tableau corresponding to the first base solution
        tableau: Tableau
            a simplex tableau corresponding to the solution 
        is_feasible: bool
            whether the problem is feasible
        is_bounded: bool
            whether the problem is bounded

        Methods
        -------
        __init__(model: smodel.Model, assignment: list[float] | None, initial_tableau: Tableau, tableau: Tableau, is_feasible: bool, is_bounded: bool) -> Solution:
            constructs a new solution for the specified model, assignment, tableau
            if the assignment is null, one of the flags should false - either the solution is infeasible or is unbounded
        assignment(model: smodel.Model | None) -> list[float] | None:
            list with the values assigned to the variables in the model if solution is feasible and bounded, otherwise None
            order of values should correspond to the order of variables in model.variables list
            if model is None, method defaults to the model attribute
        value(var: Variable) -> float:
            returns a value assigned to the specified variable if the model is feasible and bounded, otherwise None
        objective_value() -> float:
            returns a value of the objective function if the model is feasible and bounded, otherwise None
        has_assignment() -> bool:
            helper method returning info if the model is feasible and bounded, only then there is an assignment available
    
        Static Methods
        --------------
        with_assignment(model: smodel.Model, assignment: list[float], initial_tableau: Tableau, tableau: Tableau):
            helper method to creata solutions with valid assignments
        infeasible(model: smodel.Model, initial_tableau: Tableau, tableau: Tableau):    
            helper method to create infeasible solutions
        unbounded(model, initial_tableau: Tableau, tableau: Tableau):
            helper method to create unbounded solutions
    """
    model: Model
    is_feasible: bool
    is_bounded: bool
    tableau: Tableau
    initial_tableau: Tableau
    _assignment: list[float] | None

    def __init__(self, model: Model, assignment: list[float] | None, initial_tableau: Tableau, tableau: Tableau, is_feasible: bool, is_bounded: bool):
        self.model = model 
        self.is_feasible = is_feasible
        self.is_bounded = is_bounded
        self.tableau = tableau
        self.initial_tableau = initial_tableau
        self._assignment = None if assignment is None else assignment.copy()

    def assignment(self, model: Model | None = None) -> list[float] | None:
        if self._assignment is None:
            return None
        model = self.model if model is None else model
        return self._assignment[:len(model.variables)]

    def value(self, var: Variable) -> float:
        if self._assignment is None:
            raise CalledWithoutAssignmentError("cannot get a variable's value without a valid assignment")
        return self._assignment[var.index]

    def objective_value(self) -> float:
        if self._assignment is None:
            raise CalledWithoutAssignmentError("cannot get an objective value without a valid assignment")
        return self.model.objective.evaluate(self._assignment) 

    def has_assignment(self) -> bool:
        return self._assignment is not None

    @staticmethod
    def with_assignment(model: Model, assignment: list[float], initial_tableau: Tableau, tableau: Tableau):
        return Solution(model, assignment, initial_tableau, tableau, True, True)  

    @staticmethod
    def infeasible(model: Model, initial_tableau: Tableau, tableau: Tableau) -> Solution:
        return Solution(model, None, initial_tableau, tableau, False, True)   

    @staticmethod
    def unbounded(model, initial_tableau: Tableau, tableau: Tableau) -> Solution:
        return Solution(model, None, initial_tableau, tableau, True, False)

    def __str__(self, model: Model | None = None) -> str:
        model = self.model if model is None else model
        
        if not self.is_bounded:
            return "There is no optimal solution, the model is unbounded"

        if not self.is_feasible:
            return "There is no optimal solution, the model is unsolvable" 

        assert self._assignment is not None, "The assignment shouldn't be `None` for feasible, bounded solutions"

        text = f'- objective value: {self.objective_value()}\n'
        text += '- assignment:'
        for var in model.variables:
            text += f'\n\t- {var.name} = {"{:.3f}".format(self._assignment[var.index])}'
        return text