from __future__ import annotations
from copy import deepcopy
from typing import Sequence

from itertools import groupby
from functools import reduce
import saport.simplex.expressions.constraint as sconstraint


class Expression:
    """
        A class to represent a linear polynomial, i.e. a sum of atom (e.g. 4x + 5y - 0.4z)

        Attributes
        ----------
        atoms : list[Atom]
            list of atoms in the polynomial (atom is a single variable with a coefficient)

        Class Methods
        _____________
        from_vectors(variables : Iterable[Variable], coefficients: Iterable[float]) -> Expression:
            constructs an expression with collections of coefficients and corresponding variables
        
        Methods
        -------
        __init__(*atoms : *Atom) -> Expression:
            constructs an expression with atoms given in the parameter list
        evaluate(assignment: Sequence[float]) -> float:
            returns value of the expression for the given assignment
            assignment is just a list of values with order corresponding to the variables in the model,
            e.g. 2x+4y+5z has value for assignment (1,2,3) = 2 * 1 + 4 * 2 + 5 * 3 = 25, assuming
                 x has value 1, y has value 2, z has value 3 
        get_coefficients(variables: list[Variable]) -> list[float]:
            converts an expression to a flat list of the coefficients, e.g.
            - given: 
                variables, x1, x2, x3, x4
                expression 2x1 + 3x4
            - result should be
                [2.0, 0.0, 0.0, 4.0]
                as variables:
                    * x1 has coefficient 2.0
                    * x2 and x3 are missing from expression, so their coefficients are zero
                    * x4 has coefficient 4.0
            this method is most often call with model, e.g. expression1.get_coefficients(model.variables)
            warning: should be used only on the simplified expressions, otherwise the result may be incorrect
        get_coefficient(variable: Variable) -> float:
            gets a coefficient for the given variable
            warning: should be used only on the simplified expressions, otherwise the result may be incorrect
        set_coefficient(var: Variable, coeff: float):
            overrides coefficient for the given variable 
            if there is no such variable in the expression, it's get added with the given coefficient
            setting coeff to 0.0 removes variable from the expression
            warning: should be used only on the simplified expressions, otherwise the result may be incorrect
            
        simplify() -> None:
            in-place operation: sortes atoms and reduces coefficients 
        __add__(other: Expression) -> Expression:
            returns sum of the two polynomials
        __sub__(other: Expression) -> Expression:
            returns sum of the two polynomials, inverting the first atom in the second polynomial
            useful for expressions like 3*x - 4y, otherwise one would have to write 3*x + -4*y 
        __mul__(coefficient: float) -> Expression:
            return a new polynomial with all coefficients multiplied by the given number
        __eq__(bound: float) -> Constraint:
            returns a new equality constraint
        __le__(bound: float) -> Constraint:
            returns a new "less than or equal" constraint
        __ge__(bound: float) -> Constraint:
            returns a new "greater than or equal" constraint
    """
    atoms: list[Atom]

    def __init__(self, *atoms: Atom):
        self.atoms = list(atoms)

    @classmethod
    def from_vectors(cls, variables: Sequence[Variable], coefficients: Sequence[float]) -> Expression:
        assert len(variables) == len(coefficients), f"number of coefficients should correspond to variables in the " \
                                                    f"expression"
        atoms = [Atom(v, f) for (v, f) in zip(variables, coefficients)]
        return Expression(*atoms)

    def evaluate(self, assignment: Sequence[float]) -> float:
        def adder(val: float, a: Atom) -> float:
            return val + a.evaluate_with_value(assignment[a.var.index])

        return reduce(adder, self.atoms, 0.0)

    def simplify(self):
        def projection(a: Atom) -> int:
            return a.var.index

        def reduce_atoms(a1: Atom, a2: Atom) -> Atom:
            return Atom(a1.var, a1.coefficient + a2.coefficient)

        def reduce_group(g: Sequence[Atom]) -> Atom:
            return reduce(reduce_atoms, g[1:], g[0])

        sorted_atoms = sorted(self.atoms, key=projection)
        grouped_atoms = [list(g[1]) for g in groupby(sorted_atoms, key=projection)]
        self.atoms = [reduce_group(g) for g in grouped_atoms]

    def get_coefficients(self, variables: list[Variable]) -> list[float]:
        coefficients = [0.0 for _ in variables]
        for a in self.atoms:
            if a.var.index < len(coefficients):
                coefficients[a.var.index] = a.coefficient
        return coefficients

    def get_coefficient(self, var: Variable) -> float:
        for a in self.atoms:
            if a.var == var:
                return a.coefficient
        return 0.0

    def set_coefficient(self, var: Variable, coeff: float):
        self.simplify()
        matching_atoms = [a for a in self.atoms if a.var == var]

        if len(matching_atoms) == 0 and coeff != 0.0:
            self.atoms.append(Atom(var, coeff))
            return 

        if coeff == 0.0:
            self.atoms = [a for a in self.atoms if a.var != var]
            return 

        matching_atom = matching_atoms[0]
        matching_atom.coefficient = coeff

    def __add__(self, other: Expression | float) -> Expression:
        if isinstance(other, (int, float)) and other == 0:
            return Expression(*self.atoms)
        assert isinstance(other, Expression), f'expressions supports only adding 0 as a numeric value, got {other}'
        new_atoms = list(self.atoms)
        new_atoms += other.atoms
        return Expression(*new_atoms)

    __radd__ = __add__

    def __sub__(self, other: Expression) -> Expression:
        return self.__add__(other * -1)

    def __neg__(self) -> Expression:
        return self.__mul__(-1)

    def __mul__(self, factor: float) -> Expression:
        new_atoms = [a * factor for a in self.atoms]
        return Expression(*new_atoms)

    __rmul__ = __mul__

    def __eq__(self, bound: float) -> sconstraint.Constraint:  # type: ignore[override]
        return sconstraint.Constraint(self, bound, sconstraint.ConstraintType.EQ)

    def __ge__(self, bound: float) -> sconstraint.Constraint:
        return sconstraint.Constraint(self, bound, sconstraint.ConstraintType.GE)  # type: ignore

    def __le__(self, bound: float) -> sconstraint.Constraint:
        return sconstraint.Constraint(self, bound, sconstraint.ConstraintType.LE)  # type: ignore

    def __str__(self) -> str:
        text = str(self.atoms[0])

        for atom in self.atoms[1:]:
            text += ' + ' if atom.coefficient >= 0 else ' - '
            coefficient = "" if abs(atom.coefficient) == 1.0 else f"{abs(atom.coefficient)}*"
            text += f'{coefficient}{atom.var.name}'
        return text


class Atom(Expression):
    """
    A class to represent an atom of the linear programming expression, i.e. variable and it's coefficient (e.g. 4x, -5.3x,
    etc.) It derives from the Expression class and can be interpreted as an expression containing only single atom,
    itself.

        Attributes
        ----------
        var : Variable
            variable associated with the atom
        coefficient : float
            coefficient value associated with the atom

        Methods
        -------
        __init__(var: Variable, coefficient: float) -> Atom:
            constructs new atom with a specified variable and coefficient
        evaluate(assigned_value: float) -> float:
            returns value of the atom for the given assignment
        __mul__(factor: float) -> Atom:
            return new atom with a multiplied coefficient
    """
    var: Variable
    coefficient: float

    def __init__(self, var: Variable, coefficient: float):
        self.var = var
        self.coefficient = float(coefficient)
        super().__init__(self)

    def evaluate_with_value(self, assigned_value: float) -> float:
        return self.coefficient * assigned_value

    def evaluate(self, assignment: Sequence[float]) -> float:
        return super().evaluate(assignment)

    def __mul__(self, factor: float) -> Atom:
        return Atom(self.var, self.coefficient * factor)

    __rmul__ = __mul__

    def __str__(self):
        if float(self.coefficient) == 1.0:
            return str(self.var)
        elif float(self.coefficient) == -1.0:
            return f"-{self.var}"
        else:
            return f"{self.coefficient}*{self.var}"


class Variable(Atom):
    """
        A class to represent a linear programming variable.
        It derives from the Atom class and can be interpreted as an Atom with factor = 1.

        Attributes
        ----------
        name : str
            name of the variable
        index : int
            index of the variable used in the model

        Methods
        -------
        __init__(name: str, index: int) -> Variable:
            constructs new variable with a specified name and index
    """
    name: str
    index: int

    def __init__(self, name: str, index: int):
        self.name = name
        self.index = index
        super().__init__(self, 1)

    def __str__(self) -> str:
        return self.name

    def __key__(self) -> tuple[str, int]:
        return self.name, self.index

    def __hash__(self) -> int:
        return hash(self.__key__())

    def __eq__(self, other: object) -> bool:  # type: ignore[override]
        if not isinstance(other, Variable):
            return NotImplemented

        return self.__key__() == other.__key__()
