from __future__ import annotations

from copy import deepcopy
from dataclasses import dataclass
from saport.simplex.exceptions import DuplicateVariableError, EmptyModelError, MissingObjectiveError

from saport.simplex.expressions.objective import Objective, ObjectiveType
from saport.simplex.expressions.constraint import Constraint
from saport.simplex.expressions.expression import Expression, Variable


class Model:
    """
        A class to represent a linear programming problem.

        Attributes
        ----------
        name : str
            name of the problem
        variables : list[Variable]
            list with the problem variable, variable with index 'i' is always stored at the variables[i]
        constraints : list[Constraint]
            list containing problem constraints
        objective : Objective
            represents the objective function

        Methods
        -------
        __init__(name: str):
            constructs new model with a specified name
        create_variable(name: str) -> Variable
            returns a new variable with a specified named, the variable is automatically indexed and added to the variables list
            variable names have to be unique!
        add_constraint(constraint: Constraint)
            adds a new constraint to the model
        maximize(expression: Expression)
            sets objective to maximize the specified Expression
        minimize(expression: Expression)
            sets objective to minimize the specified Expression
        simplify():
            simplifies all the expressions used in the model
    """
    name: str
    variables: list[Variable]
    constraints: list[Constraint]
    objective: Objective

    def __init__(self, name: str):
        self.name = name
        self.variables = []
        self.constraints = []
        self.objective = Objective(Expression())

    def create_variable(self, name: str) -> Variable:
        for var in self.variables:
            if var.name == name:
                raise DuplicateVariableError(name)

        new_index = len(self.variables)
        variable = Variable(name, new_index)
        self.variables.append(variable)
        return variable

    def add_constraint(self, constraint: Constraint):
        constraint.index = len(self.constraints)
        self.constraints.append(constraint)

    def maximize(self, expression: Expression):
        self.objective = Objective(expression, ObjectiveType.MAX)

    def minimize(self, expression: Expression):
        self.objective = Objective(expression, ObjectiveType.MIN)

    def simplify(self) -> None:
        """ Simplifies all expressions in the model """
        for c in self.constraints:
            c.simplify()
        if self.objective is not None:
            self.objective.simplify()

    def validate(self) -> None:
        if len(self.variables) == 0:
            raise EmptyModelError()

        if self.objective is None:
            raise MissingObjectiveError()

    def expression_coefficients(self, expression: Expression) -> list[float]:
        simplified_expression = deepcopy(expression)
        simplified_expression.simplify()
        coefficients = [0.0 for _ in self.variables]
        for atom in simplified_expression.atoms:
            if atom.var.index < len(coefficients):
                coefficients[atom.var.index] = atom.coefficient
        return coefficients

    def __str__(self) -> str:
        separator = '\n\t'
        text = (f"- name: {self.name},\n"
                f"- variables: {separator}{separator.join([f'{v.name} >= 0' for v in self.variables])},\n"
                f"- constraints: {separator}{separator.join([str(c) for c in self.constraints])}\",\n"
                f"- objective: {separator}{self.objective}\n")
        return text
