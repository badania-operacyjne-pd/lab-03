from __future__ import annotations

from copy import deepcopy
import numpy as np

from saport.simplex.model import Model
from saport.simplex.tableau import Tableau
from saport.simplex.expressions.objective import Objective, ObjectiveType
from saport.simplex.expressions.constraint import Constraint, ConstraintType
from saport.simplex.expressions.expression import Expression, Atom, Variable
from saport.simplex.solution import Solution


class Solver:
    """
        A class to represent a simplex solver.

        Attributes:
        ______
        _slacks: dict[Variable, Constraint]:
            contains mapping from slack variables to their corresponding constraints
        _surpluses: dict[Variable, Constraint]:
            contains mapping from surplus variables to their corresponding constraints
        _artificial: dict[Variable, Constraint]:
            contains mapping from artificial variables to their corresponding constraints

        Methods
        -------
        solve(model: Model) -> Solution:
            solves the given model and return the first solution
    """
    _slacks: dict[Variable, Constraint]
    _surpluses: dict[Variable, Constraint]
    _artificial: dict[Variable, Constraint]

    def solve(self, model: Model) -> Solution:
        model.validate()
        normal_model = self._augment_model(model)
        if len(self._slacks) < len(normal_model.constraints):
            tableau, success = self._presolve(normal_model)
            if not success:
                return Solution.infeasible(model, tableau, tableau)
        else:
            tableau = self._basic_initial_tableau(normal_model)

        initial_tableau = deepcopy(tableau)
        if self._optimize(tableau) == False:
            return Solution.unbounded(model, initial_tableau, tableau)

        assignment = tableau.extract_assignment()
        return self._create_solution(assignment, model, initial_tableau, tableau)

    def _optimize(self, tableau: Tableau) -> bool:
        while not tableau.is_optimal():
            pivot_col = tableau.choose_entering_variable()
            if tableau.is_unbounded(pivot_col):
                return False
            pivot_row = tableau.choose_leaving_variable(pivot_col)

            tableau.pivot(pivot_row, pivot_col)
        return True

    def _presolve(self, model: Model) -> tuple[Tableau, bool]:
        """
            _presolve(model: smodel.Model) -> Tableau:
                returns a initial tableau for the second phase of simplex
        """
        presolve_model = self._create_presolve_model(model)
        tableau = self._presolve_initial_tableau(presolve_model)

        self._optimize(tableau)

        if self._artifical_variables_are_positive(tableau):
            return (tableau, False)

        tableau = self._restore_initial_tableau(tableau, model)
        return (tableau, True)

    def _augment_model(self, original_model: Model) -> Model:
        """
            _augment_model(model: smodel.Model) -> smodel.Model:
                returns an augmented version of the given model
        """
        model = deepcopy(original_model)
        model.simplify()
        self._change_objective_to_max(model)
        self._change_constraints_bounds_to_nonnegative(model)
        self._add_slack_and_surplus_variables(model)
        return model

    def _create_presolve_model(self, augmented_model: Model) -> Model:
        presolve_model = deepcopy(augmented_model)
        self._artificial = self._add_artificial_variables(presolve_model)
        return presolve_model

    def _change_objective_to_max(self, model: Model):
        if model.objective.type == ObjectiveType.MIN:
            model.objective.invert()

    def _change_constraints_bounds_to_nonnegative(self, model: Model):
        for constraint in model.constraints:
            if constraint.bound < 0:
                constraint.invert()

    def _add_slack_and_surplus_variables(self, model: Model):
        slacks: dict[Variable, Constraint] = dict()    
        surpluses: dict[Variable, Constraint] = dict() 

        for constraint in model.constraints:
            if constraint.type == ConstraintType.LE:
                slack_var = model.create_variable(f"s{constraint.index}")
                slacks[slack_var] = constraint
                constraint.expression += slack_var
                constraint.type = ConstraintType.EQ
            if constraint.type == ConstraintType.GE:
                surplus_var = model.create_variable(f"s{constraint.index}")
                surpluses[surplus_var] = constraint
                constraint.expression -= surplus_var
                constraint.type = ConstraintType.EQ 

        self._slacks = slacks
        self._surpluses = surpluses

    def _add_artificial_variables(self, model: Model) -> dict[Variable, Constraint]:
        artificial_variables: dict[Variable, Constraint] = dict()
        # TODO: add artificial variables to the model.
        #      tip 1. you may base your code on the methods: _add_slack_variables/_add_surplus_variable 
        #      tip 2. artificial variables have to be added only to the constraints without slacks
        #             - self._slacks.values() is a list of constraints where the slacks have been added
        #             - to check if a given constraint is in the list, compare its index with their indices
        #               (constraint class has an `index` attribute, you may use, e.g. c1.index == c2.index)
        return artificial_variables

    def _presolve_initial_tableau(self, model: Model) -> Tableau:
        # TODO: create an initial tableau for the artificial variables
        #       - objective row should contain 1.0 for every artificial variable
        #       - then fix the tableau basis (tip. artificial variables should be basic) using simple transformations; 
        #         like in the pivot: subtract rows / multiply by constant
        #       tip 1. you may look at the _basic_initial_tableau on how to create a tableau
        table = None
        return Tableau(model, table)

    def _basic_initial_tableau(self, model: Model) -> Tableau:
        objective_row = -1 * np.array(model.objective.expression.get_coefficients(model.variables) + [0.0])
        table = np.array([objective_row] + [c.expression.get_coefficients(model.variables) + [c.bound] for c in model.constraints])
        return Tableau(model, table)

    def _artifical_variables_are_positive(self, tableau: Tableau) -> bool:
        # TODO: check whether any artificial variable in the table is positive
        #       tip 1. `self._artificial` contains info about the artificial variables
        #       tip 2. use `tableau.extract_assignment` or `tableau.extract_basis`
        #           - `Variable` class has an `index` attribute, e.g. you may use
        #             `assignment[var.index]` to get value of the variable `var` in the assignment 
        return False

    def _restore_initial_tableau(self, tableau: Tableau, model: Model) -> Tableau:
        # TODO: remove artificial variables from the tableau and restore the objective
        #       1. remove corresponding columns from the tableau (`np.delete` is a little helper here)
        #       2. restore the original objective row
        #       3. similarly to the way we have zeroed the artificial variables in `_presolve_initial_tableau`,
        #          now we have to transform the tableau to make the basic variables (basic = being part of the basis) 
        #          in the first phase tableau also basic in the new tableau
        new_table = None
        return Tableau(tableau.model, new_table)

    def _create_solution(self, assignment: list[float], model: Model, initial_tableau: Tableau, tableau: Tableau) -> Solution:
        return Solution.with_assignment(model, assignment, initial_tableau, tableau)
    
